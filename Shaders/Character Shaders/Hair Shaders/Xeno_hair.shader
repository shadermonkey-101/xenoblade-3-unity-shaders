// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Xeno_hair"
{
	Properties
	{
		_Difusegoeshere("Difuse goes here", 2D) = "white" {}
		_Normal("Normal", 2D) = "bump" {}
		_HairMocaphere("Hair Mocap here", 2D) = "white" {}
		_FactorBlend("Factor-Blend", Range( 0 , 1)) = 0.5
		[KeywordEnum(Texture,Matcap)] _MatorTex("Mat orTex", Float) = 0
		_MatCapColor("MatCap Color", Color) = (0,0,0,0)
		_PowerCap("Power Cap", Range( 0 , 1)) = 0.4470588
		[KeywordEnum(World,Tagent)] _ViewDir("View Dir", Float) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#pragma multi_compile_local _MATORTEX_TEXTURE _MATORTEX_MATCAP
		#pragma multi_compile_local _VIEWDIR_WORLD _VIEWDIR_TAGENT
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float3 worldNormal;
			INTERNAL_DATA
			float2 uv_texcoord;
			float3 worldPos;
		};

		struct SurfaceOutputCustomLightingCustom
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			half Alpha;
			Input SurfInput;
			UnityGIInput GIData;
		};

		uniform sampler2D _Difusegoeshere;
		uniform float4 _Difusegoeshere_ST;
		uniform sampler2D _Normal;
		uniform float4 _Normal_ST;
		uniform float _FactorBlend;
		uniform sampler2D _HairMocaphere;
		uniform float4 _MatCapColor;
		uniform float _PowerCap;


		struct Gradient
		{
			int type;
			int colorsLength;
			int alphasLength;
			float4 colors[8];
			float2 alphas[8];
		};


		Gradient NewGradient(int type, int colorsLength, int alphasLength, 
		float4 colors0, float4 colors1, float4 colors2, float4 colors3, float4 colors4, float4 colors5, float4 colors6, float4 colors7,
		float2 alphas0, float2 alphas1, float2 alphas2, float2 alphas3, float2 alphas4, float2 alphas5, float2 alphas6, float2 alphas7)
		{
			Gradient g;
			g.type = type;
			g.colorsLength = colorsLength;
			g.alphasLength = alphasLength;
			g.colors[ 0 ] = colors0;
			g.colors[ 1 ] = colors1;
			g.colors[ 2 ] = colors2;
			g.colors[ 3 ] = colors3;
			g.colors[ 4 ] = colors4;
			g.colors[ 5 ] = colors5;
			g.colors[ 6 ] = colors6;
			g.colors[ 7 ] = colors7;
			g.alphas[ 0 ] = alphas0;
			g.alphas[ 1 ] = alphas1;
			g.alphas[ 2 ] = alphas2;
			g.alphas[ 3 ] = alphas3;
			g.alphas[ 4 ] = alphas4;
			g.alphas[ 5 ] = alphas5;
			g.alphas[ 6 ] = alphas6;
			g.alphas[ 7 ] = alphas7;
			return g;
		}


		float4 SampleGradient( Gradient gradient, float time )
		{
			float3 color = gradient.colors[0].rgb;
			UNITY_UNROLL
			for (int c = 1; c < 8; c++)
			{
			float colorPos = saturate((time - gradient.colors[c-1].w) / ( 0.00001 + (gradient.colors[c].w - gradient.colors[c-1].w)) * step(c, (float)gradient.colorsLength-1));
			color = lerp(color, gradient.colors[c].rgb, lerp(colorPos, step(0.01, colorPos), gradient.type));
			}
			#ifndef UNITY_COLORSPACE_GAMMA
			color = half3(GammaToLinearSpaceExact(color.r), GammaToLinearSpaceExact(color.g), GammaToLinearSpaceExact(color.b));
			#endif
			float alpha = gradient.alphas[0].x;
			UNITY_UNROLL
			for (int a = 1; a < 8; a++)
			{
			float alphaPos = saturate((time - gradient.alphas[a-1].y) / ( 0.00001 + (gradient.alphas[a].y - gradient.alphas[a-1].y)) * step(a, (float)gradient.alphasLength-1));
			alpha = lerp(alpha, gradient.alphas[a].x, lerp(alphaPos, step(0.01, alphaPos), gradient.type));
			}
			return float4(color, alpha);
		}


		inline half4 LightingStandardCustomLighting( inout SurfaceOutputCustomLightingCustom s, half3 viewDir, UnityGI gi )
		{
			UnityGIInput data = s.GIData;
			Input i = s.SurfInput;
			half4 c = 0;
			SurfaceOutputStandard s40 = (SurfaceOutputStandard ) 0;
			float2 uv_Difusegoeshere = i.uv_texcoord * _Difusegoeshere_ST.xy + _Difusegoeshere_ST.zw;
			float4 tex2DNode3 = tex2D( _Difusegoeshere, uv_Difusegoeshere );
			s40.Albedo = (tex2DNode3).rgb;
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			s40.Normal = ase_worldNormal;
			s40.Emission = float3( 0,0,0 );
			s40.Metallic = 0.0;
			s40.Smoothness = 0.0;
			s40.Occlusion = 1.0;

			data.light = gi.light;

			UnityGI gi40 = gi;
			#ifdef UNITY_PASS_FORWARDBASE
			Unity_GlossyEnvironmentData g40 = UnityGlossyEnvironmentSetup( s40.Smoothness, data.worldViewDir, s40.Normal, float3(0,0,0));
			gi40 = UnityGlobalIllumination( data, s40.Occlusion, s40.Normal, g40 );
			#endif

			float3 surfResult40 = LightingStandard ( s40, viewDir, gi40 ).rgb;
			surfResult40 += s40.Emission;

			#ifdef UNITY_PASS_FORWARDADD//40
			surfResult40 -= s40.Emission;
			#endif//40
			float4 temp_output_6_0_g4 = float4( surfResult40 , 0.0 );
			float2 uv_Normal = i.uv_texcoord * _Normal_ST.xy + _Normal_ST.zw;
			UnityGI gi43 = gi;
			float3 diffNorm43 = WorldNormalVector( i , UnpackNormal( float4( UnpackNormal( tex2D( _Normal, uv_Normal ) ) , 0.0 ) ) );
			gi43 = UnityGI_Base( data, 1, diffNorm43 );
			float3 indirectDiffuse43 = gi43.indirect.diffuse + diffNorm43 * 0.0001;
			Gradient gradient20 = NewGradient( 0, 2, 2, float4( 0, 0, 0, 0.3600061 ), float4( 1, 1, 1, 0.3900054 ), 0, 0, 0, 0, 0, 0, float2( 1, 0.002944991 ), float2( 1, 1 ), 0, 0, 0, 0, 0, 0 );
			float4 temp_output_41_12 = ( temp_output_6_0_g4 + ( ( float4( ( indirectDiffuse43 * (SampleGradient( gradient20, 0.0 )).rgb ) , 0.0 ) * temp_output_6_0_g4 ) * _FactorBlend ) );
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldViewDir = Unity_SafeNormalize( UnityWorldSpaceViewDir( ase_worldPos ) );
			float3 ase_worldTangent = WorldNormalVector( i, float3( 1, 0, 0 ) );
			float3 ase_worldBitangent = WorldNormalVector( i, float3( 0, 1, 0 ) );
			float3x3 ase_worldToTangent = float3x3( ase_worldTangent, ase_worldBitangent, ase_worldNormal );
			float3 ase_tanViewDir = mul( ase_worldToTangent, ase_worldViewDir );
			#if defined(_VIEWDIR_WORLD)
				float3 staticSwitch68 = ase_worldViewDir;
			#elif defined(_VIEWDIR_TAGENT)
				float3 staticSwitch68 = ase_tanViewDir;
			#else
				float3 staticSwitch68 = ase_worldViewDir;
			#endif
			#if defined(_MATORTEX_TEXTURE)
				float4 staticSwitch59 = temp_output_41_12;
			#elif defined(_MATORTEX_MATCAP)
				float4 staticSwitch59 = ( temp_output_41_12 + ( ( ( tex2D( _HairMocaphere, ( staticSwitch68 - float3( i.uv_texcoord ,  0.0 ) ).xy ) * tex2DNode3 ) * _MatCapColor ) * _PowerCap ) );
			#else
				float4 staticSwitch59 = temp_output_41_12;
			#endif
			c.rgb = staticSwitch59.rgb;
			c.a = 1;
			return c;
		}

		inline void LightingStandardCustomLighting_GI( inout SurfaceOutputCustomLightingCustom s, UnityGIInput data, inout UnityGI gi )
		{
			s.GIData = data;
		}

		void surf( Input i , inout SurfaceOutputCustomLightingCustom o )
		{
			o.SurfInput = i;
			o.Normal = float3(0,0,1);
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardCustomLighting keepalpha fullforwardshadows noshadow 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float4 tSpace0 : TEXCOORD2;
				float4 tSpace1 : TEXCOORD3;
				float4 tSpace2 : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				half3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputCustomLightingCustom o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputCustomLightingCustom, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18935
2035.333;365.3333;1920;1004;206.4694;180.7943;1;True;True
Node;AmplifyShaderEditor.CommentaryNode;64;-861.0975,548.2717;Inherit;False;1496.939;681.6693;Comment;8;50;53;49;54;55;63;61;68;Cap;0.6850141,0.7075472,0,1;0;0
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;69;-811.7156,775.5377;Inherit;True;Tangent;True;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;53;-809.5426,583.4804;Inherit;True;World;True;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.StaticSwitch;68;-381.5437,629.4686;Inherit;False;Property;_ViewDir;View Dir;7;0;Create;True;0;0;0;False;0;False;1;0;0;True;;KeywordEnum;2;World;Tagent;Create;True;True;All;9;1;FLOAT3;0,0,0;False;0;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;5;FLOAT3;0,0,0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;50;-803.94,956.843;Inherit;True;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleSubtractOpNode;49;-545.1121,749.6111;Inherit;True;2;0;FLOAT3;0,0,0;False;1;FLOAT2;0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;5;-1092.687,217.4863;Inherit;True;Property;_Normal;Normal;1;0;Create;True;0;0;0;False;0;False;-1;None;1f80a297fc39a094aa267edf56fb4239;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GradientNode;20;-883.6396,120.5515;Inherit;False;0;2;2;0,0,0,0.3600061;1,1,1,0.3900054;1,0.002944991;1,1;0;1;OBJECT;0
Node;AmplifyShaderEditor.SamplerNode;3;-1287.687,-139.5137;Inherit;True;Property;_Difusegoeshere;Difuse goes here;0;0;Create;True;0;0;0;False;0;False;-1;None;54f5c0f2b05aa2f4c8c675b710514f97;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;54;-257.4161,770.5414;Inherit;True;Property;_HairMocaphere;Hair Mocap here;2;0;Create;True;0;0;0;False;0;False;-1;6ccde83b9f9b2b347b5c5902bc8d9cdd;6ccde83b9f9b2b347b5c5902bc8d9cdd;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GradientSampleNode;45;-582.312,190.8262;Inherit;True;2;0;OBJECT;;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.UnpackScaleNormalNode;44;-797.312,224.8262;Inherit;False;Tangent;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.ColorNode;63;135.4993,1017.941;Inherit;False;Property;_MatCapColor;MatCap Color;5;0;Create;True;0;0;0;False;0;False;0,0,0,0;1,1,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.IndirectDiffuseLighting;43;-599.1403,73.24028;Inherit;False;Tangent;1;0;FLOAT3;0,0,1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ComponentMaskNode;47;-873.312,-122.1738;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;55;115.2204,732.3868;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ComponentMaskNode;48;-274.3828,230.3168;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;46;-65.31201,81.82617;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;42;-648.312,-211.1738;Inherit;False;Property;_FactorBlend;Factor-Blend;3;0;Create;True;0;0;0;False;0;False;0.5;0.64;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;66;823.3306,988.3397;Inherit;False;Property;_PowerCap;Power Cap;6;0;Create;True;0;0;0;False;0;False;0.4470588;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;61;400.8413,757.2482;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CustomStandardSurface;40;-616.4565,-125.5245;Inherit;False;Metallic;Tangent;6;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,1;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;65;941.3306,739.3397;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.FunctionNode;41;71.54346,-134.5245;Inherit;True;blender mutpily;-1;;4;a2f4a63cf3d4c9748b45e402f51c2a1c;0;3;8;FLOAT;0.5;False;6;COLOR;1,0,0,0;False;7;COLOR;0,0,0,0;False;1;COLOR;12
Node;AmplifyShaderEditor.CommentaryNode;70;-2113.142,1604.125;Inherit;False;3105.242;896.5239;Comment;21;91;90;89;88;87;86;85;84;83;82;81;80;79;78;77;76;75;74;73;72;71;Vertex Offset;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleAddOpNode;57;561.3309,137.6323;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StaticSwitch;59;864.4312,-0.880285;Inherit;True;Property;_MatorTex;Mat orTex;4;0;Create;True;0;0;0;False;0;False;1;0;0;True;;KeywordEnum;2;Texture;Matcap;Create;True;True;All;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;74;-1443.352,2165.78;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PannerNode;77;-1125.19,2080.798;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;1,1;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;78;-1125.646,2236.343;Inherit;False;3;0;FLOAT2;1,1;False;2;FLOAT2;1,1;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;75;-1628.926,2107.027;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;73;-2065.435,2314.822;Float;False;Property;_WindSpeedY;WindSpeed Y;11;0;Create;True;0;0;0;False;0;False;0;-0.404;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;80;-770.0831,2074.785;Inherit;False;True;False;True;True;1;0;FLOAT2;0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;71;-2068.047,2190.332;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;91;834.3959,1689.645;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;72;-2068.917,2063.673;Float;False;Property;_WindSpeedX;WindSpeed X;10;0;Create;True;0;0;0;False;0;False;0;1;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;79;-766.462,2255.716;Inherit;False;False;True;True;True;1;0;FLOAT2;0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;81;-448.293,1656.557;Float;False;Property;_DirectionX;Direction X;12;0;Create;True;0;0;0;False;0;False;0;0.01;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;82;-450.5919,1750.5;Float;False;Property;_DirectionY;Direction Y;13;0;Create;True;0;0;0;False;0;False;0;0.025;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;83;-336.0729,2083.822;Inherit;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;84;-448.2129,1850.397;Float;False;Property;_DirectionZ;Direction Z;14;0;Create;True;0;0;0;False;0;False;0;0.01;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;76;-1622.061,2255.945;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;88;-45.27485,2270.125;Inherit;True;Property;_WindGradient;WindGradient;9;1;[NoScaleOffset];Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WorldNormalVector;85;-829.454,1678.802;Inherit;False;False;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SamplerNode;86;-47.38887,2062.761;Inherit;True;Property;_WindMask;WindMask;8;1;[NoScaleOffset];Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;89;580.3411,2050.083;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;90;389.8101,1687.975;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.DynamicAppendNode;87;41.83402,1711.442;Inherit;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;1218.069,-144.8996;Float;False;True;-1;2;ASEMaterialInspector;0;0;CustomLighting;Xeno_hair;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;18;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;0;0;False;-1;0;False;-1;0;5;False;-1;2;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;True;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;68;1;53;0
WireConnection;68;0;69;0
WireConnection;49;0;68;0
WireConnection;49;1;50;0
WireConnection;54;1;49;0
WireConnection;45;0;20;0
WireConnection;44;0;5;0
WireConnection;43;0;44;0
WireConnection;47;0;3;0
WireConnection;55;0;54;0
WireConnection;55;1;3;0
WireConnection;48;0;45;0
WireConnection;46;0;43;0
WireConnection;46;1;48;0
WireConnection;61;0;55;0
WireConnection;61;1;63;0
WireConnection;40;0;47;0
WireConnection;65;0;61;0
WireConnection;65;1;66;0
WireConnection;41;8;42;0
WireConnection;41;6;40;0
WireConnection;41;7;46;0
WireConnection;57;0;41;12
WireConnection;57;1;65;0
WireConnection;59;1;41;12
WireConnection;59;0;57;0
WireConnection;77;0;74;0
WireConnection;77;1;75;0
WireConnection;78;0;74;0
WireConnection;78;1;76;0
WireConnection;75;0;71;0
WireConnection;75;1;72;0
WireConnection;80;0;77;0
WireConnection;91;0;89;0
WireConnection;91;1;90;0
WireConnection;79;0;78;0
WireConnection;83;0;80;0
WireConnection;83;1;79;0
WireConnection;76;0;71;0
WireConnection;76;1;73;0
WireConnection;86;1;83;0
WireConnection;89;0;88;2
WireConnection;89;1;86;2
WireConnection;90;0;85;0
WireConnection;90;1;87;0
WireConnection;87;0;81;0
WireConnection;87;1;82;0
WireConnection;87;2;84;0
WireConnection;0;13;59;0
ASEEND*/
//CHKSM=75E468836935D98D57142457774B6F1E3EF8CF32