// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "xeno nosedot"
{
	Properties
	{
		_Cutoff( "Mask Clip Value", Float ) = 0.5
		_Difsuemakewhitetexture("Difsue-make white texture", 2D) = "white" {}
		_Emission("Emission", Range( 0 , 1)) = 0.2
		_DotTextureHere("Dot Texture Here", 2D) = "white" {}
		_mask("mask", Range( 0 , 1)) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "AlphaTest+0" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _Difsuemakewhitetexture;
		uniform float4 _Difsuemakewhitetexture_ST;
		uniform float _Emission;
		uniform sampler2D _DotTextureHere;
		uniform float4 _DotTextureHere_ST;
		uniform float _mask;
		uniform float _Cutoff = 0.5;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_Difsuemakewhitetexture = i.uv_texcoord * _Difsuemakewhitetexture_ST.xy + _Difsuemakewhitetexture_ST.zw;
			float4 tex2DNode1 = tex2D( _Difsuemakewhitetexture, uv_Difsuemakewhitetexture );
			o.Albedo = tex2DNode1.rgb;
			o.Emission = ( tex2DNode1 * _Emission ).rgb;
			o.Alpha = 1;
			float2 uv_DotTextureHere = i.uv_texcoord * _DotTextureHere_ST.xy + _DotTextureHere_ST.zw;
			clip( ( tex2D( _DotTextureHere, uv_DotTextureHere ) * _mask ).r - _Cutoff );
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18935
2035.333;365.3333;1920;1004;1222.093;264.6726;1;True;True
Node;AmplifyShaderEditor.RangedFloatNode;2;-727.6351,-212.11;Inherit;False;Property;_Emission;Emission;2;0;Create;True;0;0;0;False;0;False;0.2;0.288;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;6;-908.1492,388.5497;Inherit;False;Property;_mask;mask;4;0;Create;True;0;0;0;False;0;False;0;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;4;-752.6351,88.89;Inherit;True;Property;_DotTextureHere;Dot Texture Here;3;0;Create;True;0;0;0;False;0;False;-1;None;9f1b02997c17bba40a3bca200ec0b38c;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;1;-753.6351,-131.11;Inherit;True;Property;_Difsuemakewhitetexture;Difsue-make white texture;1;0;Create;True;0;0;0;False;0;False;-1;None;1ac966b20e70ea44495c650a905cbdde;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;3;-446.6351,17.88995;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;5;-545.1492,358.5497;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;0,0;Float;False;True;-1;2;ASEMaterialInspector;0;0;Standard;xeno nosedot;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;True;0;True;Opaque;;AlphaTest;All;18;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;True;Relative;0;;0;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;3;0;1;0
WireConnection;3;1;2;0
WireConnection;5;0;4;0
WireConnection;5;1;6;0
WireConnection;0;0;1;0
WireConnection;0;2;3;0
WireConnection;0;10;5;0
ASEEND*/
//CHKSM=99EC8EA5CE64BF698634687462BDABE587A7944D