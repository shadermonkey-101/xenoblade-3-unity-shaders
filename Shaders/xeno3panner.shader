// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "xeno3panner"
{
	Properties
	{
		_PannerTexturehere("Panner Texture here", 2D) = "white" {}
		_Cutoff( "Mask Clip Value", Float ) = 1
		_EmissionColor("Emission Color", Color) = (1,1,1,0)
		_EmissionPower("Emission Power", Range( 0 , 100)) = 20
		_Speed("Speed", Vector) = (0,1,0,0)
		_MaskClip("Mask Clip", Range( 0 , 1)) = 0.5
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "AlphaTest+0" "IsEmissive" = "true"  }
		Cull Off
		Blend SrcAlpha OneMinusSrcAlpha , SrcAlpha OneMinusSrcAlpha
		
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha noshadow 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _PannerTexturehere;
		uniform float4 _Speed;
		uniform float _EmissionPower;
		uniform float4 _EmissionColor;
		uniform float _MaskClip;
		uniform float _Cutoff = 1;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 panner3 = ( 1.0 * _Time.y * _Speed.xy + i.uv_texcoord);
			float4 tex2DNode1 = tex2D( _PannerTexturehere, panner3 );
			float4 temp_output_11_0 = ( tex2DNode1 * ( _EmissionPower * _EmissionColor ) );
			o.Albedo = temp_output_11_0.rgb;
			o.Emission = temp_output_11_0.rgb;
			o.Alpha = 1;
			clip( ( tex2DNode1.a * _MaskClip ) - _Cutoff );
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18935
7;102;1920;909;1179.158;293.0397;1;True;True
Node;AmplifyShaderEditor.TextureCoordinatesNode;9;-1362.453,-292.53;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector4Node;2;-1360.35,-135.4113;Inherit;False;Property;_Speed;Speed;4;0;Create;True;0;0;0;False;0;False;0,1,0,0;0,0,0,0;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PannerNode;3;-1124.35,-158.4113;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;5;-654.3501,337.5887;Inherit;False;Property;_EmissionPower;Emission Power;3;0;Create;True;0;0;0;False;0;False;20;0;0;100;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;6;-606.3501,423.5887;Inherit;False;Property;_EmissionColor;Emission Color;2;0;Create;True;0;0;0;False;0;False;1,1,1,0;1,1,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;1;-839.3501,-208.4113;Inherit;True;Property;_PannerTexturehere;Panner Texture here;0;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;10;-337.9019,409.3262;Inherit;True;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;4;-767.3501,94.58871;Inherit;False;Property;_MaskClip;Mask Clip;5;0;Create;True;0;0;0;False;0;False;0.5;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;7;-379.035,77.87613;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;11;-267.9019,162.3262;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;0,0;Float;False;True;-1;2;ASEMaterialInspector;0;0;Standard;xeno3panner;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Off;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;1;True;False;0;True;Opaque;;AlphaTest;All;18;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;2;5;False;-1;10;False;-1;2;5;False;-1;10;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;True;Relative;0;Diffuse;1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;3;0;9;0
WireConnection;3;2;2;0
WireConnection;1;1;3;0
WireConnection;10;0;5;0
WireConnection;10;1;6;0
WireConnection;7;0;1;4
WireConnection;7;1;4;0
WireConnection;11;0;1;0
WireConnection;11;1;10;0
WireConnection;0;0;11;0
WireConnection;0;2;11;0
WireConnection;0;10;7;0
ASEEND*/
//CHKSM=12C722ECC9B98746CFEEBB5250FC7BB202D26FF2